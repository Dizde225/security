package sprawko.servapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {


    @GetMapping("/api/remoteConn")
    public String response()  {

        return "admin";
    }

    @GetMapping("/api/user")
    public String responseUser()  {

        return "user";
    }
}
