package sprawko.servapp;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JwtFilter extends BasicAuthenticationFilter {

    private String userInfoString = "";

    public JwtFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String fullheader = request.getHeader("Authorization");
        String header = request.getHeader("Authorization").substring(7);
        UsernamePasswordAuthenticationToken token = getUsernamePasswordAuthenticationToken(header);
        SecurityContextHolder.getContext().setAuthentication(token);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(String header) {

        com.auth0.jwt.algorithms.Algorithm algorithm = com.auth0.jwt.algorithms.Algorithm.HMAC512("klucz");
        com.auth0.jwt.JWTVerifier verifier = com.auth0.jwt.JWT.require(algorithm).build();
        com.auth0.jwt.interfaces.DecodedJWT jwt = verifier.verify(header);
        String username = jwt.getClaim("name").asString();
        String roles = jwt.getClaim("roles").asString();
        setUserInfoString(username, roles);
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(roles);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                = new UsernamePasswordAuthenticationToken(username, null, Collections.singletonList(simpleGrantedAuthority));
        return usernamePasswordAuthenticationToken;
    }


    public void setUserInfoString(String username, String roles){
        userInfoString = "name: ".concat(username).concat(" role: ").concat(roles);
    }

    public String getUserInfoString() {
        return userInfoString;
    }
}
