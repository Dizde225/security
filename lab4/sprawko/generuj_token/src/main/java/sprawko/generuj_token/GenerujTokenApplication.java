package sprawko.generuj_token;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenerujTokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenerujTokenApplication.class, args);
    }

}
