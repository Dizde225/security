package sprawko.generuj_token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private AppUserRepo appUserRepo;
    private RoleRepo roleRepo;
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    public WebSecurityConfig(AppUserRepo appUserRepo, RoleRepo roleRepo, UserDetailsServiceImpl userDetailsService) {
        this.appUserRepo = appUserRepo;
        this.roleRepo = roleRepo;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().httpBasic().and().authorizeRequests()
                .antMatchers("/Conn").authenticated()
                .antMatchers("/Role").authenticated()
                .antMatchers("/test1").authenticated()
                .anyRequest().permitAll()
                .and()
                .formLogin().permitAll();
    }

    public void newUser(String name, String pass, String role){
        AppUser appUser = new AppUser();
        appUser.setUserName(name);
        appUser.setPassword(getPasswordEncoder().encode(pass));

        Role userRole = new Role(role);
        appUser.setRoles(Collections.singletonList(userRole));
        userRole.setUsers(Collections.singletonList(appUser));

        roleRepo.save(userRole);
        appUserRepo.save(appUser);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init(){
        newUser("admin", "admin", "ROLE_ADMIN");
        newUser("user", "user", "ROLE_USER");
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return  new BCryptPasswordEncoder();
    }
}
