package sprawko.generuj_token;

import com.vaadin.flow.component.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import static org.apache.commons.lang3.ArrayUtils.toArray;

@RestController
public class RemoteApi {
    String token = "";
    @Autowired
    private AppUserRepo appUserRepo;
    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/Role")
    public String getRole(Authentication authentication){
        String roles =authentication.getAuthorities().toString();
        roles= roles.substring(1,roles.length()-1);
        return roles;
    }
    @GetMapping("/Conn")
    public String makeToken(Authentication authentication){
        try {
            com.auth0.jwt.algorithms.Algorithm algorithm = com.auth0.jwt.algorithms.Algorithm.HMAC512("klucz");
            token = com.auth0.jwt.JWT.create()
                    .withClaim("name", authentication.getName())
                    .withClaim("roles",  getRole(authentication))
                    .withIssuedAt(new Date(System.currentTimeMillis()))
                    .withExpiresAt(new Date(System.currentTimeMillis() + 3000000))
                    .sign(algorithm);
        } catch (com.auth0.jwt.exceptions.JWTCreationException exception) {
}
//
//        String result= " ";
//        RestTemplate restTemplate = new RestTemplate();
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Authorization", "Bearer " + token);
//        HttpEntity<String> entity = new HttpEntity<>("", headers);
//        try {
//            result = restTemplate.exchange("http://localhost:9090/api/remoteConn", HttpMethod.GET, entity, String.class).getBody();
//            Notification.show("token sent");
//        }
//        catch (Exception e){
//            result = e.toString();
//            Notification.show("Conn err");
//        }
        return token;
    }
}
