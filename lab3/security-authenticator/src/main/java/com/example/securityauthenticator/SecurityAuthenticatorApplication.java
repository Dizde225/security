package com.example.securityauthenticator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityAuthenticatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityAuthenticatorApplication.class, args);
    }

}
