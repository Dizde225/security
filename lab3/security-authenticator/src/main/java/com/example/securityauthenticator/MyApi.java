package com.example.securityauthenticator;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class MyApi {

    @GetMapping("/hello")
    public String hello(){
       HashMap name = (HashMap) ((OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication())
               .getUserAuthentication().getDetails();

        return "hello ".concat(name.get("name").toString());
    }
}
