package com.example.securityauthenticator;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.LinkedHashMap;

@Route("user")
public class MyView extends VerticalLayout {
    OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
    String userId = authentication.getPrincipal().toString();
    LinkedHashMap<String, Object> properties = (LinkedHashMap<String, Object>) authentication.getUserAuthentication().getDetails();

    String username = (String) properties.get("name");
    String photoUrl = "http://graph.facebook.com/" + userId + "/picture?type=square";

    Label label = new Label(username);
    Image image = new Image();

    public MyView() {
        image.setSrc(photoUrl);
        add(image);
        add(label);
    }
}