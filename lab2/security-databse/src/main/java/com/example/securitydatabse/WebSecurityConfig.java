package com.example.securitydatabse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Set;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private AppUserRepo appUserRepo;
    private RoleRepo roleRepo;
    private UserDetailsService userDetailsService;

    @Autowired
    public WebSecurityConfig(AppUserRepo appUserRepo, RoleRepo roleRepo, UserDetailsService userDetailsService) {
        this.appUserRepo = appUserRepo;
        this.roleRepo = roleRepo;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/test1").authenticated()
                .anyRequest().permitAll()
                .and()
                .formLogin().permitAll();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init(){
        AppUser appUser = new AppUser();
        appUser.setUserName("Admin");
        appUser.setPassword(getPasswordEncoder().encode("Admin"));

        Role userRole = new Role("ADMIN");
        appUser.setRoles(Collections.singletonList(userRole));
        userRole.setUsers(Collections.singletonList(appUser));

        roleRepo.save(userRole);
        appUserRepo.save(appUser);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return  new BCryptPasswordEncoder();
    }
}
