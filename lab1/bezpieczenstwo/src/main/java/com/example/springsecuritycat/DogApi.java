package com.example.springsecuritycat;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DogApi {
    List<Dog> dogs = new ArrayList<>();

    public DogApi() {
        dogs.add(new Dog("Bruno",3));
        dogs.add(new Dog("Reks",13));
    }

    @GetMapping("/dog")
    public List<Dog> getDog(){
        return dogs;
    }

    @PostMapping("/dog")
    public void post(@RequestBody Dog dog){
        dogs.add(dog);
    }
}
