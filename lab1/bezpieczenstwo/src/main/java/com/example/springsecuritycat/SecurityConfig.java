package com.example.springsecuritycat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser(
                User.builder().username("ADMIN")
                     .password(passwordEncoder().encode("ADMIN"))
                     .roles("ADMIN")
        );
        auth.inMemoryAuthentication().withUser(
                User.builder().username("USER")
                        .password(passwordEncoder().encode("USER"))
                        .roles("USER")
        );

        auth.inMemoryAuthentication().withUser(
                User.builder().username("USER1")
                        .password(passwordEncoder().encode("USER1"))
                        .roles("USER1")
        );
        auth.inMemoryAuthentication().withUser(
                User.builder().username("USER2")
                        .password(passwordEncoder().encode("USER2"))
                        .roles("USER2")
        );
        auth.inMemoryAuthentication().withUser(
                User.builder().username("USER3")
                        .password(passwordEncoder().encode("USER3"))
                        .roles("USER3")
        );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/dog").hasAnyRole("USER", "ADMIN")//
                .antMatchers(HttpMethod.GET, "/mouse").hasAnyRole("USER", "ADMIN")//
                .antMatchers(HttpMethod.POST,"/mouse").hasRole("ADMIN")//
                .antMatchers("/cat").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().permitAll()
                .and()
                .logout().permitAll();
        http.headers()
                .frameOptions()
                .disable();
    }
}
