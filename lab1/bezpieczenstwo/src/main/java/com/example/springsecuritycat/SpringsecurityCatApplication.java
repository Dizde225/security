package com.example.springsecuritycat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringsecurityCatApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringsecurityCatApplication.class, args);
    }

}
