package com.example.springsecuritycat;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CatApi {
    private List<Cat> catList;

    public CatApi()
    {
        this.catList=new ArrayList<>();
        catList.add(new Cat("fddf", 1));
        catList.add(new Cat("fgcfhgjymn", 41));
    }

    @GetMapping("cat")
    public List<Cat> get(){
        return  catList;
    }

    @PostMapping("cat")
    public boolean addCat(@RequestBody Cat cat)
    {
        return catList.add(cat);
    }

//    @DeleteMapping("/api/cat")
//    public boolean deleteCat(@RequestParam int id)
//    {
//        catList.remove(id);
//        return true;
//    }

    @GetMapping("/test1")
    public String test1()
    {
        return "test1";
    }

    @GetMapping("/test2")
    public String test2()
    {
        return "test2";
    }
}
