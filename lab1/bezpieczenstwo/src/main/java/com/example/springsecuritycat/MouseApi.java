package com.example.springsecuritycat;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
@RestController
public class MouseApi {
    List<Mouse> mouses = new ArrayList<>();

    public MouseApi() {
        mouses.add(new Mouse("Miki", 12));
        mouses.add(new Mouse("Mini",123));
    }

    @GetMapping("/mouse")
    public List<Mouse> getMouse(){
        return mouses;
    }

    @PostMapping("/mouse")
    public void post(@RequestBody Mouse mouse)
    {
        mouses.add(mouse);
    }
}
